from pydantic import BaseModel, EmailStr, Field
from datetime import date

class SalaryBase(BaseModel):
  amount: int = Field(gt=0)
  update_date: date
  
  
class SalaryCreate(SalaryBase):
  pass


class Salary(SalaryCreate):
  id: int
  user_id: int
  
  class Config:
    from_attributes = True
  

class UserBase(BaseModel):
  first_name: str
  last_name: str
  email: EmailStr


class UserCreate(UserBase):
  username: str | None = None
  password: str


class User(UserBase):
  id: int


  class Config:
    from_attributes = True


class TokenData(BaseModel):
  username: str | None = None
