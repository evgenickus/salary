from sqlalchemy.orm import Session
from passlib.context import CryptContext
from . import models, schemas

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")


def get_user(db: Session, user_id: int):
  return db.query(models.User).filter(models.User.id == user_id).first()


def get_user_by_username(db: Session, username: str):
  return db.query(models.User).filter(models.User.username == username).first()


def get_user_by_email(db: Session, email: str):
  return db.query(models.User).filter(models.User.email == email).first()


def get_users(db: Session, skip: int = 0, limit: int = 100):
  return db.query(models.User).offset(skip).limit(limit).all()


def get_salary_by_id(db: Session, user_id: int):
  return db.query(models.Salary).filter(models.Salary.user_id == user_id).first()


def get_salary_list(db: Session, skip: int = 0, limit: int = 100):
  return db.query(models.Salary).offset(skip).limit(limit).all()


def create_user(db: Session, user: schemas.UserCreate):
  hash_password = pwd_context.hash(user.password)
  db_user = models.User(
    first_name=user.first_name, last_name = user.last_name,
    email=user.email, username=user.username, hashed_password=hash_password)
  db.add(db_user)
  db.commit()
  db.refresh(db_user)
  return db_user


def create_salary_item(db: Session, salary_item: schemas.SalaryCreate, user_id: int):
  db_salary_item = models.Salary(**salary_item.model_dump(), user_id=user_id)
  db.add(db_salary_item)
  db.commit()
  db.refresh(db_salary_item)
  return db_salary_item
  