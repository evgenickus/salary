from fastapi import Depends, FastAPI, HTTPException, status
from sqlalchemy.orm import Session
from passlib.context import CryptContext
from fastapi.security import OAuth2PasswordBearer, OAuth2PasswordRequestForm
from jose import jwt, JWTError
from datetime import datetime, timedelta, timezone
from  . import crud, models, schemas
from .database import SessionLocal, engine

models.Base.metadata.create_all(bind=engine)

app = FastAPI()

#Sequrty
SECRET_KEY = "verylongsecretkey"
ALGORITHM = "HS256"
ACCESS_TOKEN_EXPIRE_MINUTES = 30

pwd_context = CryptContext(schemes=["bcrypt"], deprecated="auto")
oauth2_schemas = OAuth2PasswordBearer(tokenUrl="login")


#Dependency
def get_db():
  db = SessionLocal()
  try:
    yield db
  finally:
    db.close()


def verify_password(password, hashed_password):
  return pwd_context.verify(
    password, hashed_password)


def authenticate_user(db, username: str, password: str):
  user = crud.get_user_by_username(db, username=username)
  if not user:
    return False
  if not verify_password(password, user.hashed_password):
    return False
  return user


def create_access_token(data: dict, expires_delta: timedelta | None = None):
  to_encode = data.copy()
  if expires_delta:
    expire = datetime.now(timezone.utc) + expires_delta
    print(expire)
  else:
    expire = datetime.now(timezone.utc) + timedelta(minutes=15)
    print(expire)

  to_encode.update({"exp": expire})
  encoded_jwt = jwt.encode(to_encode, SECRET_KEY, algorithm=ALGORITHM)
  print(encoded_jwt)
  return encoded_jwt


async def get_current_user(token: str = Depends(oauth2_schemas), db: Session = Depends(get_db)):
  credentials_exception = HTTPException(
    status_code=status.HTTP_401_UNAUTHORIZED,
    detail="Could not validate credentials",
    headers={"WWW-Authenticate": "Bearer"}
  )
  try:
    payload = jwt.decode(token, SECRET_KEY, algorithms=[ALGORITHM])
    username: str = payload.get("sub")
    if username is None:
      raise credentials_exception
    token_data = schemas.TokenData(username=username)
  except JWTError:
    raise credentials_exception
  user = crud.get_user_by_username(db, username=token_data.username)
  
  if user is None:
    raise credentials_exception
  return user


#Routs
@app.post("/login", include_in_schema=False)
def login_for_get_salary(
  db: Session = Depends(get_db),
  form_data: OAuth2PasswordRequestForm = Depends()
):
  user = authenticate_user(db, form_data.username, form_data.password)
  if not user:
    raise HTTPException(
      status_code=status.HTTP_401_UNAUTHORIZED,
      detail="Incorrect username or password",
      headers={"WWW-Authenticate": "Bearer"}
    )
  access_token_expires = timedelta(minutes=ACCESS_TOKEN_EXPIRE_MINUTES)
  access_token = create_access_token(
    data={"sub": user.username}, expires_delta=access_token_expires
  )
  return {"access_token": access_token, "token_type": "bearer"}


@app.get("/salary")
def read_salary(
  current_user: schemas.User = Depends(get_current_user),
  db: Session = Depends(get_db)
):
  db_salary = crud.get_salary_by_id(db, user_id = current_user.id)
  if db_salary is None:
    raise HTTPException(status_code=404, detail="User in Salary List not found")
  return {"Сотрудник": ' '.join([current_user.first_name, current_user.last_name]), "Текущая зарплата": db_salary.amount, "Дата повышения зарплаты": db_salary.update_date }


@app.get("/salary-list/", response_model=list[schemas.Salary])
def read_salary_list(skip: int=0, limit: int=100, db: Session = Depends(get_db)):
  salary_list = crud.get_salary_list(db, skip=skip, limit=limit)

  return salary_list


@app.get("/users/", response_model=list[schemas.User])
def read_users(skip: int=0, limit: int=100, db: Session = Depends(get_db)):
  users = crud.get_users(db, skip=skip, limit=limit)
  return users


@app.post("/users/", response_model=schemas.User, status_code=201)
def create_user(user: schemas.UserCreate, db: Session = Depends(get_db)):
  db_user_username = crud.get_user_by_username(db, username=user.username)
  db_user_email =  crud.get_user_by_email(db, email=user.email)
  if db_user_username or db_user_email:
    raise HTTPException(status_code=400, detail="User already registered")
  return crud.create_user(db=db, user=user)


@app.post("/users/{username}/salary/", response_model=schemas.Salary, status_code=201)
def create_salary_item(
  username: str, salery_item: schemas.SalaryCreate, db: Session = Depends(get_db)
):
  db_user = crud.get_user_by_username(db, username=username)
  if db_user is None:
    raise HTTPException(status_code=404, detail="User is not exists")
  db_salary = crud.get_salary_by_id(db, user_id = db_user.id)
  if db_salary:
    raise HTTPException(status_code=400, detail="User already in Salary List")
  return crud.create_salary_item(db, salary_item=salery_item, user_id=db_user.id)

