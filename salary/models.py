from sqlalchemy import Column, ForeignKey, Integer, String, Date
from sqlalchemy.orm import relationship
from .database import Base


class User(Base):
  __tablename__ = "users"

  id = Column(Integer, primary_key=True, index=True)
  first_name = Column(String, index=True)
  last_name = Column(String, index=True)
  email = Column(String, unique=True, index=True)
  username = Column(String, unique=True, index=True)
  hashed_password = Column(String)

  salary = relationship("Salary", back_populates="user")


class Salary(Base):
  __tablename__ = "salary_list"

  id = Column(Integer, primary_key=True, index=True)
  amount = Column(Integer)
  update_date = Column(Date)
  user_id = Column(Integer, ForeignKey("users.id"))

  user = relationship(User, back_populates="salary")
